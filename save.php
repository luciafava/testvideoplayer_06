<?php
if (isset($_POST['nameFile'])) {
  $nameFile = htmlentities($_POST['nameFile']);
}
if (isset($_POST['title'])) {
  $title = htmlentities($_POST['title']);
}
if (isset($_POST['subtitle'])) {
  $subtitle = htmlentities($_POST['subtitle']);
}
if (isset($_POST['video'])) {
  $video = htmlentities($_POST['video']);
}
if (isset($_POST['content'])) {
  $content = htmlentities($_POST['content']);
}
if (isset($_POST['scriptName'])) {
  $scriptName = htmlentities($_POST['scriptName']);
}
if (isset($_POST['dataParams'])) {
  $dataParams = htmlentities($_POST['dataParams']);
}

$arr = array('nameFile' => $nameFile, 'title' => $title, 'subtitle' => $subtitle, 'video' => $video, 'content' => $content, 'scriptName' => $scriptName, 'dataParams' => $dataParams);
$handle = fopen("results.json", "w");
fwrite($handle, json_encode($arr));
fclose($handle);
?>