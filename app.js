function init() {
  var submitButton = document.getElementById("submit");
  submitButton.onclick = getFormData;
}
window.onload = init;

function getFormData() {
  var nameFile = document.getElementById("nameFile").value;
    if (checkInputText(nameFile, "Please enter a nameFile")) return;
  var title = document.getElementById("title").value;
    if (checkInputText(title, "Please enter a person to do the nameFile")) return;
  var subtitle = document.getElementById("subtitle").value;
    if (checkInputText(subtitle, "Please enter a subtitle")) return;
  var video = document.getElementById("video").value;
    if (checkInputText(video, "Please enter a video")) return;
  var content = document.getElementById("content").value;
    if (checkInputText(content, "Please enter a content")) return;
  var scriptName = document.getElementById("scriptName").value;
    if (checkInputText(scriptName, "Please enter a scriptName")) return;
  var dataParams = document.getElementById("dataParams").value;
    if (checkInputText(dataParams, "Please enter a dataParams")) return;

  let fd = new FormData();
  fd.append("nameFile", nameFile);
  fd.append("title", title);
  fd.append("subtitle", subtitle);
  fd.append("video", video);
  fd.append("content", content);
  fd.append("scriptName", scriptName);
  fd.append("dataParams", dataParams);
  //console.log(Array.from(fd));
  for (let obj of fd) {
    console.log(obj)
  }
  let url = "save.php";
  let xhr = new XMLHttpRequest();
  xhr.open('POST', url, true);
  /*xhr.onreadystatechange = function(ev) {
    if (xhr.readyState === 4) {
      switch (xhr.status) {
      case 200:
      case 304:
        console.log("OK or Not Modified (cached)", xhr.status);
        outputVideos(xhr.response); //responseText
        break;
      case 201:
        console.log("Created", xhr.status);
        break;
      case 403:
      case 401:
        console.log("Not Authorized or Forbidden", xhr.status);
        break;
      case 404:
        console.log("Not Found", xhr.status);
        break;
      case 500:
        console.log("Server Side Error", xhr.status);
        break;
      default:
        console.log("Some other code: ", xhr.status, xhr.status);
      }
    }
  };*/
  xhr.onerror = function(err) {
    console.warn(err);
  };
  xhr.responseType = "json";
  xhr.send(fd);

  /*function outputVideos() {
    let main = document.querySelector("main").textContent = JSON.stringify(Array.from(fd), '\t', 2);
  }*/
}
function checkInputText(value, msg) {
  if (value == null || value == "") {
  alert(msg);
  return true;
}
return false;
}